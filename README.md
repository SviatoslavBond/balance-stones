# Balance stones

## Getting started

- download the project from the repository
- go to the main branch (there is the latest working code)
- go to the /server directory
- npm i
- create a game folder in the server directory
- create desktop and mobile folders in the game directory
- put the corresponding versions of the build game in the corresponding folders
- make sure you are in server directory open terminal and run npm run dev
