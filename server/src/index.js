import "dotenv/config";
import express from "express";
import { startApp } from "./utils/startApp.js";
import { dirname } from "./dirname.js";
import path from "path";
const app = express();
app.use(express.json());
const PORT = 3030;

app.use("/desktop", express.static(path.join(dirname, "../game/desktop")));
app.use("/mobile", express.static(path.join(dirname, "../game/mobile")));

app.get("/", (req, res) => {
  const userAgent = req.headers["user-agent"];
  console.log("asdas");

  if (/iPhone|iPad|iPod/.test(userAgent)) {
    res.sendFile(path.join(dirname, "../game/mobile/"), "index.html");
    return;
  } else if (/Android/.test(userAgent)) {
    res.sendFile(path.join(dirname, "../game/mobile/"), "index.html");
    return;
  } else if (/Macintosh|Mac OS X/.test(userAgent)) {
    res.sendFile(path.join(dirname, "../game/desktop/"), "index.html");
    return;
  } else if (/Windows/.test(userAgent)) {
    res.sendFile(path.join(dirname, "../game/desktop/"), "index.html");
    return;
  }
});

startApp(app, PORT);
