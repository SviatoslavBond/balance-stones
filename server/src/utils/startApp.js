export const startApp = (app, PORT) => {
  app.listen(PORT, (err) => {
    if (err) {
      console.log("Server started with error");
    } else {
      console.log(`Server started port > ${PORT}`);
    }
  });
};
